import React from 'react';
import {
    ActivityIndicator,
    StatusBar,
    StyleSheet,
    View,
} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import firebase from 'firebase';
import User from '../User';

<script src="https://www.gstatic.com/firebasejs/8.4.1/firebase-app.js"></script>

export default class AuthLoadingScreen extends React.Component {
    constructor(props){
        super(props);
        this._bootstrapAsync();
    }

    UNSAFE_componentWillMount(){
        var firebaseConfig = {
            apiKey: "AIzaSyDNndp8cllxN5d2oISq6FgyKRadN1c-4xI",
            authDomain: "chatapp-878f4.firebaseapp.com",
            projectId: "chatapp-878f4",
            databaseURL: "https://chatapp-878f4-default-rtdb.firebaseio.com/",
            storageBucket: "chatapp-878f4.appspot.com",
            messagingSenderId: "917304558342",
            appId: "1:917304558342:web:40d262bc50828b0c0cd487",
            measurementId: "G-9K29VMVS3J"
        };
        firebase.initializeApp(firebaseConfig);
    }

    _bootstrapAsync = async () => {
        User.phone = await AsyncStorage.getItem('userPhone');

        this.props.navigation.navigate(User.phone ? 'App' : 'Auth');
    };

    render(){
        return(
            <View>
                <ActivityIndicator/>
                <StatusBar barStyle='default'/>
            </View>
        )
    }
}